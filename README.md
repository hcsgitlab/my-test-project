# my-test-project

My Test Project

```plantuml format="png" classes="uml myDiagram" alt="My super diagram placeholder" title="My super diagram" width="300px" height="300px"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
```

```@startuml
Transaccion <|-- Venta
Transaccion <|-- Compra
Transaccion *-- Detalle
Detalle *-- Producto
Importacion *-- Producto
Venta o-- Cliente
Compra o-- Proveedor
Importacion o-- ProveedorExtranjero

class Transaccion {
    fecha: Date
    vencimiento: Date
    dte: XML
    documento: PDF
}

class Venta {

}

class Compra {

}

class Detalle {
    cantidad: int
    precio: double
}

class Producto {
    nombre: String
    descripcion: String
    precio: double
}

class Cliente {
    email: String
    nombres: String
    apellidos: String
    tipo: String
    nombreFantasia: String
    razonSocial: String
    rut: RUT
    direcciones: JSON
    telefonos: JSON
}

class Proveedor {
    email: String
    website: String
    nombreFantasia: String
    razonSocial: String
    rut: RUT
    direcciones: JSON
    telefonos: JSON
    empleados: JSON
}

class ProveedorExtranjero {
    website: String
    nombreFantasia: String
    razonSocial: String
    
}

@enduml
```
